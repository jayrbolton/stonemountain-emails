Based on [Cerberus](https://tedgoas.github.io/Cerberus/)

* Main template code: `src/template/index.html`

Serve with something like `npx serve src/template`
