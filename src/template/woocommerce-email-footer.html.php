<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates/Emails
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<tr style='height: 24px;'></tr>

<!-- START footer -->
<tr>
    <td>
    <table style="width: 480px; margin: 0 auto; font-family: Montserrat, Helvetica, Arial, sans-serif; color: #0a6769; font-size: 18px;">
        <tr>
            <td style="padding: 24px 40px; background: #ffe4db;">
                <p style="margin: 0;">Have a question about your order?</p>
                <p style="margin: 8px 0 0 0;">
                    Email us at
                    <a style='font-weight: bold; color: #d1b559;' href='mailto:orders@stonemountainfabric.com'>orders@stonemountainfabric.com</a>
                </p>
            </td>
        </tr>
        <tr style="height: 12px;"></tr>
        <tr>
            <td style="padding: 24px 40px; background: #ffe4db;">
                <p style="margin: 0;">
                    For additional details, go to
                        <a style='font-weight: bold; color: #d1b559' href="<?php echo get_site_url(null, '/my-account'); ?>">your account</a>.
                </p>
            </td>
        </tr>
        <tr style="height: 12px;"></tr>
        <tr>
            <td style="padding: 24px 40px; background: #ffe4db;">
                <p style="margin: 0;">Keep in touch!</p>
                <p style="margin: 8px 0 0 0;">
                    Use 
                    <a style='font-weight: bold; color: #d1b559' href="https://www.instagram.com/explore/tags/stonemountainfabric/"> #stonemountainfabric</a>
                    to share your makes.
                </p>
                <p style='margin: 12px 0 0 0;'>
                <a style='display: inline-block; margin: 0 8px 0 0;' href='https://www.instagram.com/stonemountainfabric/'>
                <img src="<?php echo get_site_url(null, '/wp-content/uploads/email-template-assets/icon-instagram.png'); ?>" width="32" alt="Instagram" border="0" style="height: auto;">
                </a>
                <a style='display: inline-block;' href='https://www.facebook.com/StonemountainandDaughter/'>
                    <img src="<?php echo get_site_url(null, '/wp-content/uploads/email-template-assets/icon-facebook.png'); ?>" width="32" alt="Facebook" border="0" style="height: auto;">
                </a>
                </p>
            </td>
        </tr>
    </table>
    </td>
</tr>
<!-- END footer -->

<tr class='height: 32px;'></tr>

<!-- END email content -->
</table>

<!-- END div for centering/margins -->
</div>

<!--[if mso | IE]>
</td>
</tr></table>
<![endif]-->

</center>
</body>
</html>

