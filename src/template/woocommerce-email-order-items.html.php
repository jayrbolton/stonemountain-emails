<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */
defined( 'ABSPATH' ) || exit;
$text_align  = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';
$idx = 0;

foreach ( $items as $item_id => $item ) :
	$product       = $item->get_product();
	$sku           = '';
	$purchase_note = '';
	$image         = '';
    $is_first = $idx == 0;
    $idx++;

	if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		continue;
	}

	if ( is_object( $product ) ) {
		$sku           = $product->get_sku();
		$purchase_note = $product->get_purchase_note();
		$image         = $product->get_image( $image_size );
	}
?>

<tr>
    <td
    style='padding: 8px 8px 8px 0; <?php echo ($is_first ? "" : "border-top: 1px solid #71acad"); ?>'
    width='60%' valign='top'>

        <?php
		// Product name.
		echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ) );

		// SKU (only shown to admins)
		if ( $show_sku && $sku && $sent_to_admin && $sku != "swatch" ) {
			echo wp_kses_post( ' <br>(#' . $sku . ')' );
		}

		// allow other plugins to add additional product information here.
		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

		wc_display_item_meta(
			$item,
			array(
				'label_before' => '<strong class="wc-item-meta-label" style="float: ' . esc_attr( $text_align ) . '; margin-' . esc_attr( $margin_side ) . ': .25em; clear: both">',
			)
		);
		// allow other plugins to add additional product information here.
		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

		// Show quantity if it is greater than 1
		if ($item->get_quantity() > 1) {
			echo '<br><span style="font-weight: bold; padding-top: 8px; display: inline-block; font-size: 13px;">Quantity: ';
			echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $item->get_quantity(), $item ) );
			echo '</span>';
		}
		?>
    </td>

    <td
    style='padding: 8px 8px 8px 0; <?php echo ($is_first ? "" : "border-top: 1px solid #71acad"); ?>'
    width='40%' valign='top'>
        <?php echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?>
    </td>
</tr>

<?php
if ( $show_purchase_note && $purchase_note ) {
    ?>
    <tr>
        <td style='border-top: 1px solid #71acad; padding: 8px 8px 8px 0;' colspan='2'>
            <?php
            echo wp_kses_post( wpautop( do_shortcode( $purchase_note ) ) );
            ?>
        </td>
    </tr>
    <?php
}
?>

<?php endforeach; ?>
