<?php
/**
 * Email Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-addresses.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.4
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
$text_align = is_rtl() ? 'right' : 'left';
$address    = $order->get_formatted_billing_address();
$shipping   = $order->get_formatted_shipping_address();
?>

<tr>
    <td style="font-family: Montserrat, Helvetica, Arial, sans-serif; padding: 24px; background-color: #fff7f4; color: #0a6769; line-height: 26px;">
    <table style='width: 100%; font-size: 18px;'>

    <tr>
        <?php if ($shipping) : ?>
        <td valign='top'>
            <h2 style="font-family: 'Abril Fatface', 'Times New Roman', Times, serif; margin: 0 0 8px 0; font-size: 26px; line-height: 36px; color: #0a6769;">Shipping address:</h2>
            <address style='color: #0a6769; font-style: normal;'><?php echo wp_kses_post( $shipping ); ?></address>
        </td>
        <?php endif; ?>

        <td valign='top' <?php echo ($shipping ? '' : 'colspan="2"'); ?>>
            <h2 style="font-family: 'Abril Fatface', 'Times New Roman', Times, serif; margin: 0 0 8px 0; font-size: 26px; line-height: 36px; color: #0a6769;">Billing address:</h2>
            <address style='color: #0a6769; font-style: normal;'>
                <?php echo wp_kses_post( $address ? $address : esc_html__( 'N/A', 'woocommerce' ) ); ?>
                <?php if ( $order->get_billing_phone() ) : ?>
                <br/><?php echo esc_html( $order->get_billing_phone() ); ?>
                <?php endif; ?>
            </address>
        </td>
    </tr>
    </table>
    </td>
</tr>

<tr style="height: 24px"></tr>
